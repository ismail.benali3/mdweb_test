## Description 
to create this project, I use the "create react app". 
antd for ui design https://ant.design/ . 
redux , redux-persist to manage the state container
 
 ####  you can test this application with this link [https://mdweb-test.vercel.app/login]
 
 
 #### email: test@test.com
 #### password: test

## Available Scripts

In the project directory, you can run:

### `yarn`
To install node-modules

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

made with love by ismail benali 
