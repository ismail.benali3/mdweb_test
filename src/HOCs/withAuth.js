import React, {useEffect, useState} from 'react'
import {shallowEqual, useSelector} from 'react-redux'
import {useHistory} from 'react-router-dom'

// eslint-disable-next-line import/no-anonymous-default-export
export default (ComposedComponent) => (props) => {
    const history = useHistory()
    const session = useSelector((state) => state.Session, shallowEqual)
    const [loading, setLoading] = useState(true)

    console.log(session);
    useEffect(() => {
        if (session.isAuthenticated) {
            setLoading(false)
        } else {
            history.push('/login')
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [session.isAuthenticated])

    if (loading) {
        return null;
    } else {
        return <ComposedComponent {...props} />
    }
}
