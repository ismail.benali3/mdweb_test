import * as React from 'react';
import './head.css'
import TopMenu from "../menu/TopMenu";

function Head({title}) {

    return <div className="site-page-header">
        <div style={{display: 'flex', alignItems: 'center'}}>
            <h4 style={{marginLeft: 10}}>
                TODO LIST
            </h4>
            <TopMenu/>
        </div>
    </div>

}

export default Head
