import {useState} from 'react'
import {Menu} from "antd";
import {useLocation} from "react-router-dom";
import {shallowEqual, useDispatch, useSelector} from "react-redux";
import {$logout} from "../../reducers/Session.slice";

function TopMenu() {

    const location = useLocation()
    const dispatch = useDispatch()
    const session = useSelector((state) => state.Session, shallowEqual)
    // eslint-disable-next-line no-unused-vars
    const [current, setCurrent] = useState(location.pathname)

    return <Menu mode="horizontal" selectedKeys={[current]} >
        <Menu.Item key="/login">
            Home
        </Menu.Item>

        {session.isAuthenticated &&

        <Menu.Item key="/tasks">
            Tàches
        </Menu.Item>
        }

        {session.isAuthenticated &&
        < Menu.Item onClick={() => dispatch($logout())} key="logout">
            Déconnexion
        </Menu.Item>
        }
         </Menu>
}

export default TopMenu
