import React from "react";
import {Button, Form, Input} from "antd"
import {useDispatch} from "react-redux";
import {$saveTask} from "../../../reducers/Tasks.slice";
import './createTask.css'

function CreateTask() {
    const [form] = Form.useForm();
    const dispatch = useDispatch()
    const onFinish = (values) => {
        dispatch($saveTask(values))
        form.resetFields()
        console.log('Success:', values);
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };
    return (
        <div className={'createTasksContainer'}>
            <h2 className={'createTasksTitle'}>Créer une nouvelle tàche</h2>
            <Form
                form={form}
                className={'createTasksForm'}
                layout={"horizontal"}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
            >
                <Form.Item
                    className={'createTasksFormItem'}
                    labelAlign={'left'}
                    label="Nom de la tache"
                    name="name"
                    rules={[{required: true, message: 'Veuillez entrer le nom de la tache'}]}
                >
                    <Input/>
                </Form.Item>
                <Form.Item
                    label="description de la tache en une ligne"
                    labelAlign={'left'}
                    className={'createTasksFormItem'}
                    name="description"
                    rules={[{required: true, message: 'Veuillez entrer la description de la tache '}]}
                >
                    <Input/>
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit">
                        Ajouter la tàche
                    </Button>
                </Form.Item>
            </Form>
        </div>
    )
}

export default CreateTask
