import {List, Tag, Tooltip, Typography} from "antd"
import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {$removeTask, $updateTask, selectTasks} from "../../../reducers/Tasks.slice";

function ListTasks() {
    const tasks = useSelector(selectTasks)
    const dispatch = useDispatch()
    const doUpdate = (data) => {
        dispatch($updateTask({data}))
    }

    return (
        <div>
            <h2>Liste des tàche</h2>
            <List
                bordered
                dataSource={tasks}
                renderItem={item => (
                    <List.Item>
                        <div>
                            <span onChange={e => doUpdate({
                                ...item,
                                name: e.target.childNodes[0] && e.target.childNodes[0].data
                            })}>
                                <Typography.Text strong onChange={e => console.log(e)}
                                                 editable>{item.name}</Typography.Text> :
                            </span>
                            <span onChange={e => doUpdate({
                                ...item,
                                description: e.target.childNodes[0] && e.target.childNodes[0].data
                            })}>
                            <Typography.Text editable>{item.description}</Typography.Text>
                                {'  - '}
                            </span>
                            {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                            <a href={'#'} onClick={() => dispatch($removeTask(item.id))}>
                                Supprimer
                            </a>
                        </div>

                        <div style={{cursor: 'pointer'}}>
                            <Tooltip
                                title={item.completed ? 'cliquez ici si cette tâche n\'est pas terminée' : 'cliquez ici si cette tâche est terminée'}>
                                {item.completed ?
                                    <Tag color="#87d068"
                                         onClick={() => doUpdate({...item, completed: false})}>complétée</Tag> :
                                    <Tag color="#f50" onClick={() => doUpdate({...item, completed: true})}>non
                                        complétée </Tag>
                                }
                            </Tooltip>
                        </div>
                    </List.Item>
                )}
            />
        </div>
    )
}

export default ListTasks
