import React from 'react'
import {Button, Form, Input, message} from 'antd';
import {useDispatch, useSelector} from "react-redux";
import {useHistory} from "react-router-dom"
import './login.css'
import Head from "../../components/head/head";
import {$login, selectSessionLoading} from "../../reducers/Session.slice";


function LoginPage() {

    const dispatch = useDispatch();
    const loading = useSelector(selectSessionLoading)
    const history = useHistory()
    const onFinish = async ({email, password}) => {
        if (email === 'test@test.com' && password === 'test') {
            await dispatch($login(email));
            history.push('/tasks')
        } else
            message.error('email ou mot de passe incorrect')
    };
    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };
    return (
        <div>
            <Head title={'TODO LIST'}/>
            <Form
                className={'loginForm'}
                name="loginForm"
                layout="vertical"
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
            >
                <div>
                    <Form.Item

                        label="Adresse e-mail"
                        name="email"
                        rules={[{required: true, message: 'Veuillez entrer votre e-mail !'}]}
                    >
                        <Input type={'email'}/>
                    </Form.Item>

                    <Form.Item
                        label="Mot de passe"
                        name="password"
                        rules={[{required: true, message: 'Veuillez entrer votre mot de passe !'}]}
                    >
                        <Input.Password/>
                    </Form.Item>

                    <Form.Item>
                        <Button loading={loading} type="primary" htmlType="submit">
                            Soumettre
                        </Button>
                    </Form.Item>
                </div>
            </Form>
        </div>)
}

export default LoginPage
