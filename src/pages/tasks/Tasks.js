import Head from "../../components/head/head";
import React from "react";
import CreateTask from "../../components/tasks/createTask/CreateTask";
import ListTasks from "../../components/tasks/listTasks/ListTasks";
import {Divider} from "antd";
import './tasks.css'

function Tasks(props) {
    return (
        <div>
            <Head/>
            <div className={'tasksContainer'}>
                <ListTasks/>
                <Divider/>
                <CreateTask/>
            </div>
        </div>
    )
}

export default Tasks
