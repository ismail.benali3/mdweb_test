import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';

/**
 * Helper for the redundant initial state
 */
const getInitialState = () => ({email: null, loading: false, isAuthenticated: false});

/**
 * Creates a login session
 */
export const $login = createAsyncThunk('Session/login', async (email) => {
    return email
});

// Session Slice
const Session = createSlice({
    name: 'Session',
    initialState: getInitialState(),
    reducers: {
        $logout() {
            const stateUpdate = getInitialState();
            stateUpdate.isAuthenticated = false;
            localStorage.setItem('authToken', null);
            return stateUpdate;
        },
    },
    extraReducers: {
        // Login ($login)
        [$login.fulfilled]: (state, action) => {
            const stateUpdate = {...state};
            const {email} = action.payload;
            // Persist auth
            localStorage.setItem('authToken', email);
            stateUpdate.email = email;
            stateUpdate.isAuthenticated = true;
            stateUpdate.loading = false;
            return stateUpdate;
        },
        [$login.pending]: (state) => {
            const stateUpdate = {...state};
            stateUpdate.loading = true;
            return stateUpdate;
        },
        [$login.rejected]: (state) => {
            console.log('rejected');
            const stateUpdate = {...state};
            stateUpdate.loading = false;
            return stateUpdate;
        },
    },
});

export default Session.reducer;

// Simple actions
export const {$logout} = Session.actions;

// Selectors
export const selectSessionEmail = (state) => state.Session.email;
export const selectSessionLoading = (state) => state.Session.loading;
