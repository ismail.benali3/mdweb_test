import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import {uid} from "uid"

/**
 * Helper for quickly resetting state
 */
const getInitialState = () => ({tasks: [], loading: false});

/**
 * Saves a new task
 */
export const $saveTask = createAsyncThunk('Task/save', async ({name, description}) => {
    return {
        name,
        description,
        id: uid(),
        completed: false
    }
});
/**
 * Update a task
 */
export const $updateTask = createAsyncThunk('Task/update', async ({data}) => {
    return {...data}
});
/**
 * Update a task
 */
export const $removeTask = createAsyncThunk('Task/remove', async (id) => {
    return {id}
});

// Task Slice
const Tasks = createSlice({
    name: 'Tasks',
    initialState: getInitialState(),
    reducers: {},
    extraReducers: {
        //add task
        [$saveTask.fulfilled]: (state, action) => {
            const stateUpdate = {...state};
            stateUpdate.tasks = [action.payload, ...stateUpdate.tasks]
            stateUpdate.loading = false;
            return stateUpdate;
        },
        [$saveTask.pending]: (state) => {
            const stateUpdate = {...state};
            stateUpdate.loading = true;
            return stateUpdate;
        },
        // update task
        [$updateTask.pending]: (state) => {
            const stateUpdate = {...state};
            stateUpdate.loading = true;
            return stateUpdate;
        },
        [$updateTask.fulfilled]: (state, action) => {
            const stateUpdate = {...state};
            stateUpdate.tasks = [action.payload, ...stateUpdate.tasks.filter(task => task.id !== action.payload.id)]
            stateUpdate.loading = false;
            return stateUpdate;
        },
        // remove task
        [$removeTask.pending]: (state) => {
            const stateUpdate = {...state};
            stateUpdate.loading = true;
            return stateUpdate;
        },
        [$removeTask.fulfilled]: (state, action) => {
            const stateUpdate = {...state};
            stateUpdate.tasks = [...stateUpdate.tasks.filter(task => task.id !== action.payload.id)]
            stateUpdate.loading = false;
            return stateUpdate;
        },
    },
});

export const selectTasks = (state) => state.Tasks.tasks;
export const selectTasksLoading = (state) => state.Tasks.loading;

export default Tasks.reducer;
