import React from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import { routes } from './routes'

// eslint-disable-next-line import/no-anonymous-default-export
export default () => {
	return (
		<Switch>
			{routes.map((val, key) => (
				<Route strict exact {...val} key={key} />
			))}
			<Redirect from='/' to='/tasks' exact />
			<Redirect to="/tasks" />
		</Switch>
	)
}
