import LoginPage from '../pages/loginPage/LoginPage'

import {
	PATH_LOGIN,
	PATH_TASKS,
} from '../constants/paths'
import withAuth from '../HOCs/withAuth'
import Tasks from "../pages/tasks/Tasks";

export const routes = [
	{
		path: PATH_LOGIN,
		component: LoginPage,
	},

	{
		path: PATH_TASKS,
		component: withAuth(Tasks),
	},

]
