import { combineReducers } from 'redux';

import Session from '../reducers/Session.slice';
import Tasks from '../reducers/Tasks.slice';

export default combineReducers({
  Session,
  Tasks,
});
